#include <stdlib.h>
#include <stdio.h>
#include "deque.h"

tkalade *sukurti(const unsigned int kortu) {
    tkalade *kaladeP;
    //paprasau atminties ir gautos atminties adresa
    //priskiriu rodyklei
    kaladeP = (tkalade *)malloc(sizeof (*kaladeP));
    //inicijuoju, kad siuksliu nebutu
    kaladeP->kortu = 0;
    kaladeP->virsutine = NULL;
    kaladeP->apatine = NULL;
    int i;
    for (i = 0; i < kortu; i++)
        pridetiVirsuj(kaladeP, 0); //naujas kortas dedu nuo virsaus
    return kaladeP;
}

void naikinti(tkalade *kaladeP) {
    int i;
    for (i = kaladeP->kortu; i > 0; i--)
        nuimtiVirsuj(kaladeP); //naikinu ir nuo virsaus
    free(kaladeP);
}

unsigned short pridetiVirsuj(tkalade *kaladeP, const int verte) {
    tkorta *nauja;
    nauja = (tkorta*)malloc(sizeof (*nauja));
    nauja->verte = verte;
    if (nauja == NULL) { //jei sistema nedave atminties
        return 0;
    } else {
        if (kaladeP->kortu != 0) { //jei jau buvo kita korta
            (kaladeP->virsutine)->virsuj = nauja;
            nauja->apacioj = kaladeP->virsutine;
            nauja->virsuj = NULL;
            kaladeP->virsutine = nauja;
        } else {
            kaladeP->virsutine = nauja;
            kaladeP->apatine = nauja;
            nauja->virsuj = NULL;
            nauja->apacioj = NULL;
        }
        kaladeP->kortu++;
	return 1;
    }
}

unsigned short pridetiApacioj(tkalade *kaladeP, const int verte) {
    tkorta *nauja;
    nauja = malloc(sizeof (*nauja));
    nauja->verte = verte;
    if (nauja == NULL) {
        return 0;
    } else {
        if (kaladeP->kortu != 0) {
            (kaladeP->apatine)->apacioj = nauja;
            nauja->virsuj = kaladeP->apatine;
            kaladeP->apatine = nauja;
        } else {
            kaladeP->virsutine = nauja;
            kaladeP->apatine = nauja;
            nauja->virsuj = NULL;
            nauja->apacioj = NULL;
        }
        kaladeP->kortu++;
	return 1;
    }
}

unsigned short nuimtiVirsuj(tkalade *kaladeP) {
    if (kaladeP->kortu) { //jei kortu skaicius teigiamas
        struct Korta *temp;
	//mums reikia dvieju virsutiniu
        temp = (kaladeP->virsutine)->apacioj;
        free(kaladeP->virsutine);
        kaladeP->virsutine = temp;
        if(kaladeP->virsutine != NULL)
		//ties siuo sakiniu nuluztu programa,
		//jei kaladeP->virsutine niekur nerodytu
        	(kaladeP->virsutine)->virsuj = NULL;
	if(kaladeP->kortu == 1) {
		kaladeP->virsutine = NULL;
		kaladeP->apatine = NULL;
	}
        kaladeP->kortu--;
	return 1;
    } else {
        return 0;
    }
}

unsigned short nuimtiApacioj(tkalade *kaladeP) {
    if (kaladeP->kortu) {
        struct Korta *temp;
        temp = (kaladeP->apatine)->virsuj;
        free(kaladeP->apatine);
        kaladeP->apatine = temp;
	if(kaladeP->apatine != NULL)
        	(kaladeP->apatine)->apacioj = NULL;
        if(kaladeP->kortu == 1) {
		kaladeP->virsutine = NULL;
		kaladeP->apatine = NULL;
	}
        kaladeP->kortu--;
	return 1;
    } else {
        return 0;
    }
}

int virsausReiksme(const tkalade *kaladeP) {
    if(kaladeP->virsutine != NULL) {
         return (kaladeP->virsutine)->verte;
    }
    else {
	 return 0;
    }
}

int apaciosReiksme(const tkalade *kaladeP) {
    if(kaladeP->apatine != NULL) {
	 return (kaladeP->apatine)->verte;
    } else {
	 return 0;
    }
}

unsigned int kortu(tkalade *kaladeP) {
	return kaladeP->kortu;
}

