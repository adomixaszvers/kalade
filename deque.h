#ifndef DEQUE_H
#define DEQUE_H
typedef struct Korta {
    struct Korta *virsuj;
    struct Korta *apacioj;
    int verte;
} tkorta;

typedef struct Kalade {
    struct Korta *virsutine;
    struct Korta *apatine;
    unsigned int kortu;
} tkalade;

//kalades konstruktorius
tkalade *sukurti(const unsigned int kortu);
//kalades destruktorius
void naikinti(tkalade *kaladeP);

unsigned short pridetiVirsuj(tkalade *kaladeP, const int verte);
unsigned short pridetiApacioj(tkalade *kaladeP, const int verte);

//unsigned short int arTuscia(tkalade *kaladeP);

unsigned short nuimtiVirsuj(tkalade *kaladeP);
unsigned short nuimtiApacioj(tkalade *kaladeP);

int virsausReiksme(const tkalade *kaladeP);
int apaciosReiksme(const tkalade *kaladeP);

unsigned int kortu(tkalade *kaladeP);

#endif
