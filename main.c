#include <stdlib.h>
#include <stdio.h>
#include "deque.h"

int main() {
    tkalade *kaladeP;
    int *verte;
    kaladeP = sukurti(0);
    pridetiVirsuj(kaladeP, 5);
    printf("Apatine korta %d\n", apaciosReiksme(kaladeP));
    printf("Virsutine korta %d\n", virsausReiksme(kaladeP));
    pridetiVirsuj(kaladeP, 6);
    printf("Dabar yra %d kortu.\n", kortu(kaladeP));
    printf("Apatine korta %d\n", apaciosReiksme(kaladeP));
    printf("Virsutine korta %d\n", virsausReiksme(kaladeP));
    pridetiApacioj(kaladeP, 7);
    printf("Apatine korta %d\n", apaciosReiksme(kaladeP));
    printf("Virsutine korta %d\n", virsausReiksme(kaladeP));
    //nuimtiApacioj(kaladeP);
    //nuimtiApacioj(kaladeP);
    //nuimtiApacioj(kaladeP);
    //pridetiVirsuj(kaladeP, 56);
    //pridetiVirsuj(kaladeP, 100);
    nuimtiVirsuj(kaladeP);
    printf("Dabar yra %d kortu.\n", kortu(kaladeP));
    printf("Apatine korta %d\n", apaciosReiksme(kaladeP));
    printf("Virsutine korta %d\n", virsausReiksme(kaladeP));
    //naikinti(kaladeP);
    //printf("Dabar yra %d kortu.\n", kortu(kaladeP));
    //printf("Apatine korta %d\n", apaciosReiksme(kaladeP));
    //printf("Virsutine korta %d\n", virsausReiksme(kaladeP));
    
    return 0;
}
